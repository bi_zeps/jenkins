#! /bin/bash -e

# Launcher script to wrap jenkins.sh
# It provides the availability to install the initial plugins before jenkins is startet.
# For maintenance reason this is a separate script which wrapps `jenkins.sh` rather than modifying it.
# Features:
# - Installs plugins defined by `INITIAL_PLUGINS` into `$JENKINS_HOME/plugins`
#   * Format according `jenkins-plugin-cli`
# - Starts `jenkins.sh` afterwards with the provided arguments
# - Does NOT install plugins if folder is not empty

PLUGIN_DOWNLOAD_DIR=${JENKINS_HOME}/plugins

if ! [[ -d "${PLUGIN_DOWNLOAD_DIR}" ]] || [ -z "$(ls -A ${PLUGIN_DOWNLOAD_DIR})" ]; then
  # If plugins folder is empty
  if ! [[ -z "${INITIAL_PLUGINS}" ]]; then
    jenkins-plugin-cli --plugin-download-directory ${PLUGIN_DOWNLOAD_DIR} --plugins ${INITIAL_PLUGINS}
  fi

  if ! [[ -z "${INITIAL_PLUGINS_FILE}" ]]; then
    jenkins-plugin-cli --plugin-download-directory ${PLUGIN_DOWNLOAD_DIR} --plugin-file ${INITIAL_PLUGINS_FILE}
  fi
fi

exec /sbin/tini -- /usr/local/bin/jenkins.sh "$@"

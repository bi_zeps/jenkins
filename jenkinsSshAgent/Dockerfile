# MIT License
#
# Copyright (c) 2019-2022 Fabio Kruger and other contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Modified official docker-ssh-agent Dockerfile
# See: https://github.com/jenkinsci/docker-ssh-agent

ARG CPU_ARCH=x86_64
ARG BASE_IMG_VERSION=3.21.0

ARG DOWNLOAD_DIR=/tmp/download

# Multistage: downloader
FROM alpine:latest as downloader

# Common arguments must be repeated but kept empty to be accepted
ARG CPU_ARCH
ARG DOWNLOAD_DIR

## Library and client versions
ARG DOCKER_CLIENT_VERSION=27.4.1

# Prepare download dirs
ARG TMP_DOWNLOAD_DIR=/tmp
RUN mkdir -p ${DOWNLOAD_DIR} &&\
    mkdir -p ${TMP_DOWNLOAD_DIR}/resources &&\
    mkdir -p ${DOWNLOAD_DIR}/dockerBinaries

# Prepare `known_hosts` file
COPY /resources/ ${TMP_DOWNLOAD_DIR}/resources/ 
RUN touch ${DOWNLOAD_DIR}/known_hosts \
    && cat ${TMP_DOWNLOAD_DIR}/resources/known_hostsGithub >> ${DOWNLOAD_DIR}/known_hosts \
    && cat ${TMP_DOWNLOAD_DIR}/resources/known_hostsGitLab >> ${DOWNLOAD_DIR}/known_hosts

# Download docker client to connect from docker container to a docker host
ARG DOCKER_CLIENT_URL=https://download.docker.com/linux/static/stable
RUN mkdir -p ${TMP_DOWNLOAD_DIR}/dockerBinaries
RUN wget ${DOCKER_CLIENT_URL}/${CPU_ARCH}/docker-${DOCKER_CLIENT_VERSION}.tgz -P ${TMP_DOWNLOAD_DIR}
RUN tar xzvf ${TMP_DOWNLOAD_DIR}/docker-${DOCKER_CLIENT_VERSION}.tgz -C ${DOWNLOAD_DIR}/dockerBinaries

FROM alpine:${BASE_IMG_VERSION}

# Common arguments must be repeated but kept empty to be accepted
ARG DOWNLOAD_DIR

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000
ARG AGENT_PORT=22

# Agent directories and paths
ENV JENKINS_AGENT_HOME=/home/${user}
ENV AGENT_WORKDIR=/var/agent_workdir
ENV AGENT_PORT=${AGENT_PORT}
ENV JENKINS_AGENT_SSH_PUBKEY=

# Variables to configure remote docker daemon connection
# SSH config 'Host': The connection name in ssh config for the remote docker host
ENV DOCKER_REMOTE_SSH_CONNECTION_NAME=remote-docker-host
# SSH config 'HostName': Remote docker connection, can be a URL or IP
# Implicitly sets env `DOCKER_HOST` to configure docker client SSH connection
# If empty, local docker daemon is used
ENV DOCKER_REMOTE_HOST_NAME=
# SSH config 'User': User name to use for remote SSH docker connection
ENV DOCKER_REMOTE_USER=
# SSH config 'IdentityFile': Path to the SSH (private) key file to use for the remote SSH docker connection
ENV DOCKER_REMOTE_KEYFILE_PATH=/run/secrets/docker_ssh_key
# See docker documentation, env variable for docker client to connect to a specific remote docker daemon
ENV DOCKER_HOST=ssh://${DOCKER_REMOTE_SSH_CONNECTION_NAME}

RUN apk update && apk add --no-cache \
    bash \
    git=2.47.1-r0 \
    git-lfs=3.6.0-r0 \
    less=668-r0 \
    netcat-openbsd=1.226.1.1-r0 \
    openjdk21-jre=21.0.5_p11-r0 \
    openssh=9.9_p1-r2 \
    patch=2.7.6-r10

# Setup agent directories and paths
RUN mkdir -p ${JENKINS_AGENT_HOME} &&\
    mkdir -p ${JENKINS_AGENT_HOME}/.ssh &&\
    mkdir -p ${JENKINS_AGENT_HOME}/.jenkins &&\
    mkdir -p ${AGENT_WORKDIR}

# Setup agent user, group
# `addgroup`: Set the home directory (h), set user and group id (u, G), set the shell, don't ask for password (D)
RUN addgroup -g ${gid} ${group} &&\
    adduser -h ${JENKINS_AGENT_HOME} -u ${uid} -G ${group} -s /bin/bash -D ${user} &&\
    chown -R ${uid}:${gid} ${JENKINS_AGENT_HOME} &&\
    chown -R ${uid}:${gid} ${AGENT_WORKDIR}
    
# Setup and configure SSH server
RUN sed -i /etc/ssh/sshd_config \
        -e "s|#PermitRootLogin.*|PermitRootLogin no|" \
        -e "s|#PasswordAuthentication.*|PasswordAuthentication no|" \
        -e "s|#SyslogFacility.*|SyslogFacility AUTH|" \
        -e "s|#LogLevel.*|LogLevel INFO|" \
        -e "s|#PermitUserEnvironment.*|PermitUserEnvironment yes|" \
        -e "s|#PidFile.*|PidFile ${JENKINS_AGENT_HOME}/sshd.pid|"

# Alpine's ssh doesn't use $PATH defined in /etc/environment, so we define `$PATH` in `~/.ssh/environment`
# The file path has been created earlierby `mkdir -p` and also sshd was configured to
# allow environment variables to be sourced (see `sed` command related to `PermitUserEnvironment`)
RUN echo "PATH=${PATH}" >> ${JENKINS_AGENT_HOME}/.ssh/environment
COPY resources/launcher.sh /opt/launcher.sh
RUN chmod +x /opt/launcher.sh

# Add common git services as known hosts
COPY --from=downloader --chown=${user}:${group} ${DOWNLOAD_DIR}/known_hosts ${JENKINS_AGENT_HOME}/.ssh/known_hosts

# Install docker client to connect to a docker host from the container
COPY --from=downloader ${DOWNLOAD_DIR}/dockerBinaries/docker/docker /usr/local/bin/docker

VOLUME ${AGENT_WORKDIR} 
VOLUME ${JENKINS_AGENT_HOME}

WORKDIR ${JENKINS_AGENT_HOME}

EXPOSE ${AGENT_PORT}

USER ${user}

ENTRYPOINT ["sh", "-c"]
CMD ["/opt/launcher.sh"]
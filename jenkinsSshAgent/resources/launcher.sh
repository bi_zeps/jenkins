#!/bin/sh

# Initialize SSHD service to accept connections from remote client
init_sshd() {
  # Generate host key files, if they do not exist
  if [ ! -f ${JENKINS_AGENT_HOME}/id_rsa ]; then
    echo "create id rsa"
    ssh-keygen -f ${JENKINS_AGENT_HOME}/id_rsa -q -P ""
  fi

  # Add jenkins agent ssh public key to known hosts, allows to connect with private key
  # Only add the key if it is not yet in the file
  if ! grep -q "${JENKINS_AGENT_SSH_PUBKEY}" "${JENKINS_AGENT_HOME}/.ssh/authorized_keys"; then
    echo "Copy authorized key"
    echo ${JENKINS_AGENT_SSH_PUBKEY} > "${JENKINS_AGENT_HOME}/.ssh/authorized_keys"
  fi
}

# Initialize docker client to connect to remote daemon if needed
init_remote_docker() {
  echo "Enter init_remote_docker"

  # If no remote docker host is defined, do not initialize
  if [[ -z "${DOCKER_REMOTE_HOST_NAME}" ]]; then
    echo "No docker host name: ${DOCKER_REMOTE_HOST_NAME}"
    return 0
  fi

  # Setup docker connection, if not yet setup
  if ! grep -q "${DOCKER_REMOTE_HOST_NAME}" "~/.ssh/config"; then
    echo "Setup docker ssh connection"
    # Configure ssh client to connect to remote host
    tee -a ~/.ssh/config << END
Host ${DOCKER_REMOTE_SSH_CONNECTION_NAME}
  HostName ${DOCKER_REMOTE_HOST_NAME}
  User ${DOCKER_REMOTE_USER}
  IdentityFile ${DOCKER_REMOTE_KEYFILE_PATH}
END
  fi

  # Setup docker host as known host, if not yet set
  if ! grep -q "${DOCKER_REMOTE_HOST_NAME}" "~/.ssh/known_hosts"; then
    echo "Register docker host as known host"
    ssh-keyscan ${DOCKER_REMOTE_HOST_NAME} >> ~/.ssh/known_hosts
  fi
}

# Script main entry point
init_sshd
init_remote_docker

# Start sshd, not detached (-D), log to stderr (-e), specific port (-p)
exec /usr/sbin/sshd -h ${JENKINS_AGENT_HOME}/id_rsa -D -e -p ${AGENT_PORT}